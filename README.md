# g_mmpbsa_docker

Delivers [g_mmpbsaA](http://rashmikumari.github.io/g_mmpbsa/) via a Docker container.

## Installation

Easiest via pip:

```
pip install git+https://bitbucket.org/claughton/g_mmpbsa_docker.git
```
This installs two scripts in your path:

* g_mmpbsa
* energy2bfac 

## Usage

Both commands work just like the normal installed versions of the software: see [here](http://rashmikumari.github.io/g_mmpbsa/) for details.

Note: the tools/ folder contains copies of the associated Python utilities as included in  the g_mmpbsa website/git repository.

## License

Please see the web pages for [g_mmpbsa](http://rashmikumari.github.io/g_mmpbsa/)  for the license details.


