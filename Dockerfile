FROM ubuntu:latest
WORKDIR /usr/local
RUN apt-get update && apt-get install -y tar curl && \
    curl -O https://rashmikumari.github.io/g_mmpbsa/package/GMX51x/g_mmpbsa.tar.gz &&\
    tar xzf g_mmpbsa.tar.gz && \
    rm g_mmpbsa.tar.gz
RUN ln -s /usr/local/g_mmpbsa/bin/g_mmpbsa /usr/local/bin/g_mmpbsa
RUN ln -s /usr/local/g_mmpbsa/bin/energy2bfac /usr/local/bin/energy2bfac
RUN echo '#!/bin/bash' > /usr/local/bin/run && echo 'exec $@' >> /usr/local/bin/run && chmod +x /usr/local/bin/run
ENTRYPOINT ["/usr/local/bin/run"]
WORKDIR /wd
CMD []
