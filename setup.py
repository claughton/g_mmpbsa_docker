from setuptools import setup, find_packages
setup(
    name = 'g_mmpbsadocker',
    version = '5.1',
    packages = find_packages(),
    scripts = [
        'scripts/g_mmpbsa',
        'scripts/energy2bfac',
    ],
)
