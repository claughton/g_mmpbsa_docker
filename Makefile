PREFIX ?= /usr/local
VERSION = "5.1"

all: install

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 scripts/g_mmpbsa $(DESTDIR)$(PREFIX)/bin/g_mmpbsa
	install -m 0755 scripts/energy2bfac $(DESTDIR)$(PREFIX)/bin/energy2bfac

uninstall:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/g_mmpbsa
	@$(RM) $(DESTDIR)$(PREFIX)/bin/energy2bfac
	@docker rmi claughton/g_mmpbsa:$(VERSION)

build: 
	@docker build -t claughton/g_mmpbsa:$(VERSION) -f Dockerfile . 
	@docker tag claughton/g_mmpbsa:$(VERSION) claughton/g_mmpbsa:latest

publish: build
	@docker push claughton/g_mmpbsa:$(VERSION) 
	@docker push claughton/g_mmpbsa:latest 

.PHONY: all install uninstall build publish 
